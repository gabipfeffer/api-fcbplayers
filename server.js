
const express = require('express');
const knex = require('knex');
const app = express();
const port = process.env.PORT || 3000;

const bodyParser = require('body-parser');
const cors = require('cors');
app.use(cors());
app.use(bodyParser.json());


const database = knex({
  client: 'pg',
  connection: {
    host : '127.0.0.1',
    user : 'gabrielpfeffer',
    password : '',
    database : 'fcb-players'
  }
});

app.post('/add', (req, res) => {
  const { id, name, status, seasons, jersey, image, playerUrl } = req.body;
  database('players')
    .returning('*')
    .insert({
    id: id,
    name: name,
    status: status,
    seasons: seasons,
    jersey: jersey,
    image: image,
    playerurl: playerUrl
  })
    .then(user => res.json(user))
    .catch(err => res.status(400).json('unable to register player'));
})

app.get('/', (req, res) => {
  database.select('*').from('players')
    .then(data => res.json(data))
    .catch(err => res.status(400).json('unable to get players'));
})

app.get('/profile/:id', (req, res) => {
  const { id } = req.params;
  database('players').where('id', id)
    .then(player => res.json(player[0]))
    .catch(err => res.status(400).json('unable to get player'));
})

app.delete('/delete/:id', (req, res) => {
  const { id } = req.params;
  database('players')
    .where({ id: id })
    .delete()
    .then(response => res.json('player deleted succesfully'))
    .catch(err => res.status(400).json('unable to delete player'));
})

app.put('/edit/:id', (req, res) => {
  const { id } = req.params;
  const { name, status, seasons, jersey, image, playerUrl } = req.body;
  database('players')
    .returning('*')
    .where({ id: id })
    .update({
      id: id,
      name: name,
      status: status,
      seasons: seasons,
      jersey: jersey,
      image: image,
      playerurl: playerUrl
    })
    .then(response => res.json('player edited succesfully'))
    .catch(err => res.status(400).json('unable to edit'));
})

app.listen(port, () => console.log(`server is listening on port ${port}`));
